+++
title = "MongoDB Technical Interview"
outputs = ["Reveal"]

[reveal_hugo]
theme = "night"
highlight_theme = "solarized-dark"
slide_number = false
transition = "convex"
custom_css = "css/custom.css"

+++

# E-commerce transaction API

### Community engagement plan

---

## Agenda

<p class="fragment">Community engagement strategies</p>
<p class="fragment">Content sharing and direction</p>
<p class="fragment">Feedback collection and sharing</p>
<p class="fragment">Mentoring to up-level the team</p>
<p class="fragment">Measuring success</p>
<p class="fragment">Q&A</p>

{{% note %}}

{{% /note %}}

---

## Community engagement strategies

- Online platforms (GitHub, Stack Overflow, Discourse)
  * Write content around most frequent / top questions
  * Post content in Discourse forum as FAQs, "Tip of the Week", etc.


{{% note %}}
Engaging with the community is vital, especially with a new initiative, feature, etc. **Online platforms** are great for this, but they can be a bit limited because we don't own the properties, and especially with StackOverflow changing over the years. It's important to engage with the broader community, and places like **Discourse** are a great place to share ideas and get feedback, especially with the added SEO benefits. Utilizing those platforms to gather the top questions or most frequent and then posting FAQs, Tips, etc. in Discourse and potentially as blogs raises the awareness, drives engagement, and promotes our own properties.
{{% /note %}}

---

## Community engagement strategies

- Online platforms (GitHub, Stack Overflow, Discourse)
- Conference talks, workshops

{{% note %}}
**Conference talks and workshops** are an important way to raise awareness of what we've released and practical ways to use it. This is also a valuable way to get actionable, specific feedback from the developer community, which we'll touch on later. Many companies send their engineering teams to conferences to learn, and then bring back info to share, which helps drive adoption.
{{% /note %}}

---

## Community engagement strategies

- Online platforms (GitHub, Stack Overflow, Discourse)
- Conference talks, workshops
- Partner communities
  * Meetups, user groups, etc.
  * Forums and chat platforms
  * Workshops, talks, etc.

{{% note %}}
And then utilizing our **Partner communities** creates win-win opportunites. We can do this through the form of meetups, user groups, forums, workshops, talks, etc. This helps grow our awareness and reach within our Partner's communities, while also helping raise the awareness of them within ours.
{{% /note %}}

---

## Content sharing and direction

- Content from internal sources shared as:
  * FAQs, Quick starts, videos for Docs, etc. 

{{% note %}}
When I think about content sharing, I'm thinking about the **different sources** internal to the company. This would be things like support articles, and other internal resources. These are great sources of content, and can be shared with the broader community as FAQs, Quick starts, quick videos which showcase how to use the feature, and other tips and tricks
{{% /note %}}

---

## Content sharing and direction

- Content from internal sources
- Webinars, demos with partners

{{% note %}}
**Webinars and demos** are also great ways to get content out on , especially when working with a partner.
{{% /note %}}

---

## Content sharing and direction

- Content from internal sources
- Webinars, demos with partners
- Advanced concept blog posts which drive impact

{{% note %}}
**Checking with Product** on what features, endpoints, etc. drive lasting impact ensures that the content is useful, and trackable. 
{{% /note %}}

---

## Content sharing and direction

- Content from internal sources
- Webinars, demos with partners
- Advanced concept blog posts which drive impact
- Multi-format content

{{% note %}}
And finally, whatever is created should be done with **multi-format** in mind. A blog post should be a video or two and then should be a tweet, a conference talk, etc.
{{% /note %}}

---

## Feedback collection and sharing

- Identify desired feedback (Product, Engineering, etc.)</p>
- Capture specific feedback (name, company, etc)</p>
- Regularly propagate to desired channels internally</p>
- Complete feedback loop back to the community</p>

{{% note %}}

{{% /note %}}

---

## Mentoring to up-level the team

- </p>
- </p>
- </p>
- </p>

{{% note %}}

{{% /note %}}

---

## Measuring success

- </p>
- </p>
- </p>
- </p>

{{% note %}}

{{% /note %}}

---

## Q&A