// update the below to reflect your github repo location
module gitlab.com/jerdog/mongo-technical-interview

go 1.21.4

// run "hugo mod get -u github.com/dzello/reveal-hugo" to update to the latest theme
require github.com/dzello/reveal-hugo v0.0.0-20231215085953-9311d6eac7c7 // indirect
